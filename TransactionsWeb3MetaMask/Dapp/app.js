var Dapp ={};
 
// DAPP hello world voting [part1]
$(document).ready(function() {
  
   

  // Utility Pad Zeroes
 // @baseText the text wanting to be padded
 // @width with the total string side
 // @side - left||right padding for the zeros
 pad = function(baseText, width,side ='right') {
   let padChar ='0';
   
   baseText = baseText + '';

   let tmpArr = new Array(width - baseText.length + 1).join(padChar);

   return  baseText.length >= width ? baseText : side === "right" ?  baseText + tmpArr   :   tmpArr + baseText;
  }

// Utility Promisify
// Wrapper function to turn web3 calls into into promise
  promisify = (inner) =>
  new Promise((resolve, reject) =>
      inner((err, res) => {
       if (err) { 
        //console.log("<<promise err>>")
        reject(err) 
       }
      
       //console.log("<<resolving promise>>")
       resolve(res);
    })
  ); 


  // This starts the whole DAPP
  Dapp.InitEthProvider(); 
})



 

Dapp.InitEthProvider = function(){
	var me = this;

	me.metaMask      = {};

  me.targetNetwork = "15"; 

  me.url           = "http://127.0.0.1:8546"; 
 

  // first check status
  if ( Dapp.isMetaMaskInstalled() )
  {

    Dapp.areAccountsUnLocked()
        .then((res)=>{ 

           Dapp.displayAccountInfo();

           Dapp.getAndDisplayCurNetworkInfo()
               .then(()=>{  
 
                   if (Dapp.isTargetNetwork())
                   { 
                       Dapp.enableMetaMask()
                           .then((res)=>{ 
                                 
                                Dapp.initMetaMaskBindings();

                                Dapp.init();  

                                console.log ('enableMetaMask() res:',res); 
                           })

                           .catch((err)=>{  

                               console.log ('enableMetaMask err:',err); 
                           }); 
                   }
                   else
                   {    
                         // wrong network
                         Dapp.disableApplication();
                   }
               })

              .catch( (res)=>{

                   console.log("areAccountsUnLocked() err() res",res);
              }) 

        })

        .catch( (res)=>{ 

           console.log("areAccountsUnLocked() err() res",res)
        });

  }
  else
  { 
      alert ("MetaMask is not installed, which is needed for this Dapp");
  }
 

 
}





/* Init Mask Bindings - add events handlers to MetaMask built in callback mechanism*/

Dapp.initMetaMaskBindings = function(){

   // Genearl CallBack Binding for Metamask Update Event
   web3.currentProvider.publicConfigStore.on('update', function(){ 
      
     console.log("MetaMask Update():", arguments[0].selectedAddress, arguments[0].networkVersion);
 
      Dapp.setNetworkName(arguments[0].networkVersion);

      Dapp.displayAccountInfo(arguments[0].selectedAddress);
 
   });



} 

/* this checks to see if we have new approach of window.ethereum object or
   the legacy web3 approach*/

Dapp.isMetaMaskInstalled = function(){
  var me = this; 
  
  console.log('Dapp.isMetaMaskInstalled()');

  if (!me.metaMask)  me.metaMask={};

   if (window.ethereum)
   { ;
   	  console.log(' window.ethereum- approach')
      me.metaMask.installed   = web3.currentProvider.isMetaMask;
      me.metaMask.hasProvider = true;
      return me.metaMask.installed;
   }
   // Legacy Dapp
   else if (window.web3) 
   {
 	    console.log(' window.web3 - approach');
 	    me.metaMask.hasProvider = true;
 	    me.metaMask.installed   = false;
      return me.metaMask.installed;
 }
    // Non-dapp browsers...
   else 
   {
 	   console.log(' No web3');
 	   me.metaMask.installed   = false;
     me.metaMask.hasProvider = false;
     return me.metaMask.installed;
  }
	//


}

/*
 
 As of MetaMask v4.14.0, the provider is already available at window.ethereum
 and it exposes the new enable method. Calling this method today won’t trigger
 an approval popup and accounts will continue to be exposed to dapps until 
 November 2nd
*/
Dapp.enableMetaMask = async function( ){
    var result;

    try
    { 
      //console.log("calling ethereum.enable()");
      result = await ethereum.enable();  
     
      //console.log(" result:",result);

      return  result;
    } 
    catch(err) 
    {  
       //console.log(" err:",err);
       return err; 
    }
 
}

/*Call Back on Network was changed*/
Dapp.isTargetNetwork = function(){ 
   var me = this;

   console.log("Dapp.verifyNetwork()");
 
   // Check network\
   console.log(" " + me.metaMask.networkId , me.targetNetwork);

   $('#network').html("<b>Network:</b>" + me.metaMask.networkName); 

   if (me.metaMask.networkId !== me.targetNetwork )
   {
      alert ("Sorry, Incorrect Network is connect, please connect to private network:" + me.url);
       
      return false;
   }
    
   return true;
}



Dapp.displayAccountInfo = function(account){
    var me = this;
    
    account ? me.metaMask.accounts = account : null;

    $('#account').html(" <b>account:</b>" + me.metaMask.accounts); 

    $("#metamask").html(JSON.stringify(me.metaMask)); 

}
  


Dapp.getAndDisplayCurNetworkInfo = async function(){ 
   var me =this;  me.metaMask = me.metaMask || {}; 
  
   try
   {
      var result = await promisify( cb=>web3.version.getNetwork(cb) );
      
      Dapp.setNetworkName(result);

      return result;

    }
    catch(err)
    { 
       return err;
    }
 
}


 
Dapp.setNetworkName = function(netId){
  var me = this,network;

   console.log('Dapp.setNetworkName() netId:',netId)
   me.metaMask.networkId = netId;

    switch ("" + netId) { 
      case "1":
        network= 'mainnet'
        break;

      case "2":
       network='Morden';
        break;

      case "3":
       network= 'Ropsten';
        break;

      case "4":
        network= "Rinkeby";
        break;

      case "42":
        network = "Kovan";
        break;

      default:
        network=`Private ${netId}`
    } 

  
    me.metaMask.networkName = network;


    $('#network').html("<b>Network:</b>" + network);  

    return  me.metaMask.networkName;
} 

 
Dapp.areAccountsUnLocked = async function(){
  var me = this;
  
   console.log("Dapp.areAccountsLocked() promise");
   
   me.metaMask = me.metaMask || {}; 
 
  // conver web3 cqll to promnise
   try
   {
      var result = await promisify( cb=>web3.eth.getAccounts(cb) );
     
      console.log("Dapp.areAccountsUnLocked() result: ", result);
      me.metaMask.locked    = false;
      me.metaMask.accounts  = result[0]; 
      return result;

    }
    catch(err)
    {
      console.log("Dapp.areAccountsUnLocked() err: ", err);

       me.metaMask.locked    = true;
       me.metaMask.accounts  = []; 
       return err;
    }

    
  
}



Dapp.disableApplication = function(){

   $("#dapp").css({"opacity":"0"});

}



Dapp.init = function(){
  var me   = this,myContactInstance;

  console.log("Dapp.init()")
   
   // creation of contract object
   me.contract = {address:"0x38e840d1818a51330d625ddf0ef1de5d07d98506",
                  abi:[{constant:false,inputs:[{name:"candidate",type:"bytes32"}],name:"voteForCandidate",outputs:[],payable:false,stateMutability:"nonpayable",type:"function"},{inputs:[{name:"_candidateNames",type:"bytes32[]"}],payable:false,stateMutability:"nonpayable",type:"constructor"},{constant:true,inputs:[{name:"",type:"uint256"}],name:"candidateList",outputs:[{name:"",type:"bytes32"}],payable:false,stateMutability:"view",type:"function"},{constant:true,inputs:[{name:"candidate",type:"bytes32"}],name:"totalVotesFor",outputs:[{name:"",type:"uint8"}],payable:false,stateMutability:"view",type:"function"},{constant:true,inputs:[{name:"candidate",type:"bytes32"}],name:"validCandidate",outputs:[{name:"",type:"bool"}],payable:false,stateMutability:"view",type:"function"},{constant:true,inputs:[{name:"",type:"bytes32"}],name:"votesReceived",outputs:[{name:"",type:"uint8"}],payable:false,stateMutability:"view",type:"function"}]}
 
   myContactInstance    = web3.eth.contract(me.contract.abi);
   me.contract.instance = myContactInstance.at(me.contract.address); 

   me.candidates = ["Rama","Nick","Jose"];

  // Get Vote Tallies for each candiate, convert strings to hex
   me.showCandidatesVotes();

  // Fade up
  $("#dapp").css({"opacity":"1"});
}
 

Dapp.showCandidatesVotes = function(){

  var me = this, 
      candidateHex = me.candidates.map(function(el){ return web3.fromAscii(el)});

  // Get values from from contract instance (not the use of 'let' Is crucal for scoping)
  for (let i = 0; i < candidateHex.length; i++) {
    
     let hexName = candidateHex[i];
     
     me.contract.instance.totalVotesFor(hexName,(err,res)=>{  
       if (!err){
            let voteTotal = res.toNumber();
            $(`#candidate-${i+1}`).html(voteTotal);
        } 
      }); 

    } 
}

Dapp.voteForCandidate = function(candidateName){
  var me = this,gas;

  candidateName =  candidateName || $("#candidate").val();

  if (me.candidates && me.candidates.indexOf(candidateName) === -1 ){
    alert ("choose a legit candidate name above")
    return;
  }

  candidateName = web3.fromAscii(candidateName);
  
  console.log ("voting for: ",candidateName );
  
 // "ABI Byte String" Encoding Step 1. the Method ID: This is derived as the first 4 bytes 
 // of the Keccak hash of the ASCII form of the function signature
 // step 2. concat with Param data (Strip the leading "0x" part and pad with zeroes up to 32 bytes)
 // see more here-->  https://solidity.readthedocs.io/en/latest/abi-spec.html

 var methodString  = "voteForCandidate(bytes32)",
     methodID      =  web3.sha3(methodString).substr(0,10);

 var encodedInput  =  candidateName.substr(2,candidateName.length);
     encodedInput  =  pad(encodedInput,64,"right");

 var tx            = {to: Dapp.contract.address, from: Dapp.metaMask.accounts, data: methodID + encodedInput};

 
  console.log('tx',tx);
  
  // Estimate Gas and Perfrom Transaction
   //var cb = function(err,res){ !err ? gas = res : gas = null };

   //web3.eth.estimateGas(tx,cb );
    
    (async function() {  

        try
        { 
           //console.log("sending gas async call)");
           const res = await promisify(cb => web3.eth.estimateGas(tx,cb) );

           //placeTheVote();

           // Or the alternative way using ES6 Way
           promiseCB =   (res) => {console.log("promoise callback res:",res)}; 
           promisify(cb => web3.eth.sendTransaction( tx, cb) ).then(promiseCB);

        } 
        catch(e) 
        { 
           //console.log('!asyncerror:', e);
        }
 
    })();
  

    // async Call for Voting transaction
    var placeTheVote =  async function() {  
        try
        { 
           console.log("sending voting async call)");

           var result = await promisify(cb => web3.eth.sendTransaction( tx, cb) );
           
           console.log("vote result:",result );
        } 
        catch(e) 
        { 
           console.log('!asyncerror:', e);
        }
 
    } 
   
  // Perform Transaction (asy )
   //var cb = (err,res) => {!err ? console.log("res",res) : console.log('err',err)}
  //web3.eth.sendTransaction( tx, cb);
  
 
}

 