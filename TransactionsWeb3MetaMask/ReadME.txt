Web3js and MetaMask

This exploration of Metamask and Web3JS is based on an articles below
descriving metamask and the new privacy mode  Currently Dapps rely on web3.js injected unto page via metaMask but new requirement changes that, you havre to request permision to use accounts, this will be explored

Notes uses MetaMask Version 5.0.2

Related articles: 

 https://medium.com/crowdbotics/building-ethereum-dapps-with-meta-mask-9bd0685dfd57
 https://github.com/MetaMask/metamask-extension/issues/5335
 https://github.com/ethereum/wiki/wiki/JavaScript-API#contract-methods
 https://medium.com/metamask/

BREAKING CHANGES (no accounts exposed by default:

 https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
 https://eips.ethereum.org/EIPS/eip-1102

JAVASCRIPT AWAIT :

 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await

SETUP:
=====


    Dependancies needed 
    ---------------------------------------------------------------


    Node's "Http-Server" (install it globally) to allow us to serve pages (install with NPM) 

    (see http://jasonwatmore.com/post/2016/06/22/nodejs-setup-simple-http-server-local-web-server)

    You will also need to run "npm install" this locally
    (this will install web3.) which will create node_modules folder
   
    Running private EHTEREUM NODE:
    ------------------------------------------------
    
    _Ethereum-Private2 folder contain the private blockchain and accounts
    note: you will need to install GETH if you dont have it. This private blockchain  also has the the smartcontract "Voting.sol" deployed on it.

    To start the node,open bash tab, and run  bash scripts for single node use "runThisNode.sh" see below for example:
 
	

    Start your private Node and start mining:
	-----------------------------------------

	start running private test net (your path will vary):

	$ cd /Users/salpalacios/Library/_Ethereum-Private2/scripts

	$ ./runThisNode.sh

    // In another Tab run this (to attach to running node)

    $ geth attach http://127.0.0.1:8546

    // Now you can run calls agains the running node:

         > eth.accounts[0]
        "0x67a75548590d95724e9a3ea50f31a63a072564b3"

        > eth.getBalance(eth.accounts[0])
        132000000000000000000

        > eth.defaultAccoount 
        undefined

        > eth.coinbase
        "0x67a75548590d95724e9a3ea50f31a63a072564b3"


    // Or  use this (which prevenst empty blocks from being mined)

	$geth attach http://127.0.0.1:8546 --preload "minePendingOnly.js"

    $miner.start()




    Start  DAPP with http-server in console:
	-------------------------------------
    
    $cd myDappPath/

    $cd Dapp

    $http-server
   
    $node

     

