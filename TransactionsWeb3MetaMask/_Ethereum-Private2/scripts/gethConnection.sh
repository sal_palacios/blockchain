#starts geth as a servive
geth --identity "MyPrivateNet2" \
  --port 30304 \
  --rpcaddr 127.0.0.1 \
  --rpcport 8546 \
  --datadir "/Users/salpalacios/Library/_Ethereum-Private/" \
  --rpc \
  --rpcapi "eth,web3,miner,admin,personal,net" \
  --verbosity 6 \
  --rpccorsdomain "*" \
  --nodiscover \
  --networkid 15
