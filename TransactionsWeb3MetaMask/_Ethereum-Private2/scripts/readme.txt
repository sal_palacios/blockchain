

Tutorial is here:
================

Practical steps for go-ethereum setup by Aung Maw

(https://medium.com/infrageth/practical-steps-for-go-ethereum-setup-27b8d64903fc)



Dependancies needed to download and install (solk and web3.js)
---------------------------------------------------------------

To set this up run:
  >npm init
  >npm install web3@0.20.2 
  >npm install solc

this will create packack.json and node_modules folder


Running the node (two options):
---------------------------------------
The tutorial part1 runns the contact in ganche, ive expanded it to run in local private network


 _Etheriu-Private2 (gethConnections.sh):
 Note this uses GENESIS.JSON from other directory in order to be able to be loaded as secondary node

 _Etheriu-Private2 (runThisNode.sh):
 runs current node

 

Mining with Util
----------------
 mindPendingOnly.js:

 To run a geth node with preload script utility for mining (will not mine empty blocks)
 $geth attach http://127.0.0.1:8546 --preload "minePendingOnly.js"

You now run things like this

> eth.accounts[0]
"0x67a75548590d95724e9a3ea50f31a63a072564b3"

> eth.getBalance(eth.accounts[0])
132000000000000000000

> eth.defaultAccoount 
undefined

> eth.coinbase
"0x67a75548590d95724e9a3ea50f31a63a072564b3"
 
 

=====================================================
// INTERACTIONS UTILITY MODULE: Open this win Node.js console
module to be able to interact with smart contracts
you can use Remix to deploy a contract

see ( interactions/utils.js)
=====================================================
  

Description:
------------
 
This module is meant to be run inside Node.js console,so run:

$node

Deployed Contarcatd Folder:

    deployedContacts/:

      in The current Directory where you want to run this,
      utils code, it expects a relative directory to exists named 'deployedContracts/'
      with a config json file names 'deployed.json', in the same folder
      have a copy of the deployed contract.


    JSON format for deployed.json:

      [{  "contracFile":"TestStorage.sol",
          "contractName":"TestStorage",
          "Address":"0xc6a19eebb02555cfc21153464a0047f959cf3845"}]



Start your testNet:
------------------

start running private test net:

> cd /Users/salpalacios/Library/_Ethereum-Private2 

salpalacios _Ethereum-Private2 $ ./runThisNode.sh

if contract not deployed:
  From Remix (copy contract TestStorage.sol and deploy it if not already);

if contract IS deployed:
  In remix got to Run tab, select TesStrorage at address field.
   copy address of already deployed address in json (deployed.json)
   and paste into at address field in remix.


*Rember if you want to interact with contract in Remix you hace to Unlock Account



 
Getting Started with Interactions Utils Module:
---------------- ------------------------------

Assuming you private newtwork is running, in this case
its http://127.0.0.1:8546, where 8546 is rpc port.


Include this module and start working :

 $cd interactions/

 $node

  > utils =require("./utils.js")
 
  > utils.loadModules("http://127.0.0.1:8546")

  > utils.createContracts()

  > utils.unlockAccount("Dude_123");



  // TEST STORAGE APP INTERACTION
  --------------------------------

  > utils.TestStorage.addMemFunds({from: web3.eth.accounts[0],value:"900000000"})
    '0xecff147de427d6510c00db5c0657a20ea731bd1306dc8d4141c6841fd739ef5c'


 // VOTINGS APP INTERACTION
 --------------------------

   // get byte32 values of names (names where created on deploy in Remix)

   > [ "Rama","Nick", "Jose"].map((i)=>{return web3._extend.utils.toHex(i)})
   [ '0x52616d61', '0x4e69636b', '0x4a6f7365' ]

    > utils.Voting.totalVotesFor(1).toNumber()
    0


	> utils.Voting.totalVotesFor("0x4e69636b")
	BigNumber { s: 1, e: 0, c: [ 2 ] }

	> utils.Voting.totalVotesFor("0x4e69636b").toNumber()
	2

    // must set this before voting:
    >web3.eth.defaultAccount = web3.eth.coinbase

    > utils.unlockAccount("Dude_123")true

    // Voter for nick:
	> utils.Voting.voteForCandidate("0x4e69636b")
	'0xc2c4a515ef2ef11b788d560dcacf1ef120800b56440629a1939dc83df582495c'
 
   > utils.Voting.totalVotesFor("0x4e69636b").toNumber()
     5

   // got to have a callback for this

   > utils.showLatestTransaction((tx)=>{console.log(tx)})

  
*/

 